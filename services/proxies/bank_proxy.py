import requests
from django.db import transaction
from rest_framework.exceptions import ValidationError

from payments.models import Payment
from utils import choices
from wallets.models import Wallet, WalletLog


class BankProxy:
    BASE_URL = "https//www.fake-bank.com/api"
    GATEWAY_URI = "/charge/?invoice_token={invoice_token}&amount={amount}"
    STATUS_SUCCESS = "success"
    SUCCESS_URL = "https//www.fake.com/success/"
    FAILED_URL = "https//www.fake.com/failed/"

    def create_invoice(self, payment):
        deposit_url = f"{self.BASE_URL}" \
                      f"{self.GATEWAY_URI.format(invoice_token=payment.invoice_token, amount=payment.amount)}"
        return {
            "invoice_token": payment.invoice_token,
            "amount": payment.amount,
            "deposit_url": deposit_url
        }

    def check(self, invoice_token):
        response = requests.post(
            "http://127.0.0.1:8010/check"
        )
        amount = 1000
        return {
            "status": response.json().get("data"),
            "amount": amount,
        }

    def confirm_invoice(self, data):
        amount = data.get("amount")
        invoice_token = data.get("invoice_token")
        status = data.get("status")
        if amount is None or invoice_token is None or status is None:
            raise ValidationError("data is not valid")
        invoice_token = str(invoice_token)
        with transaction.atomic():
            payment = Payment.objects.filter(
                invoice_token=invoice_token, status=choices.IN_PROGRESS
            ).select_for_update().first()
            if payment is None:
                raise ValidationError("invalid invoice token")
            if payment.amount != amount:
                return self.confirm_failed_payment(payment=payment)
            check_data = bank_proxy.check(invoice_token=invoice_token)
            checked_amount = check_data.get("amount")
            checked_status = check_data.get("status")
            if checked_status != status or checked_amount != amount:
                return self.confirm_failed_payment(payment=payment)

            if status == self.STATUS_SUCCESS:
                return self.confirm_success_payment(payment=payment)

            else:
                return self.confirm_failed_payment(payment=payment)

    def confirm_success_payment(self, payment):
        if payment.transaction_type == choices.TRANSACTION_TYPE_WITHDRAW:
            return self.handle_successful_withdraw_payment(payment=payment)
        return self.handle_successful_deposit_payment(payment=payment)

    def confirm_failed_payment(self, payment):
        if payment.transaction_type == choices.TRANSACTION_TYPE_WITHDRAW:
            return self.handle_failed_withdraw_payment(payment=payment)
        return self.handle_failed_deposit_payment(payment=payment)

    def handle_failed_deposit_payment(self, payment):
        payment.status = choices.FAILED
        payment.save(update_fields=["status"])
        return self.FAILED_URL

    def handle_failed_withdraw_payment(self, payment):
        payment.status = choices.FAILED
        payment.save(update_fields=["status"])
        return self.FAILED_URL

    @transaction.atomic
    def handle_successful_withdraw_payment(self, payment):
        # for sender
        sender_wallet = Wallet.objects.select_for_update().filter(id=payment.sender_wallet.id).first()
        sender_wallet.withdraw(amount=payment.amount)
        WalletLog.objects.create(wallet=sender_wallet, amount=payment.amount, payment=payment)

        # for receiver
        receiver_wallet = Wallet.objects.select_for_update().filter(id=payment.receiver_wallet.id).first()
        receiver_wallet.deposit(amount=payment.amount)
        WalletLog.objects.create(wallet=receiver_wallet, amount=payment.amount, payment=payment)

        # update payment status
        payment.status = choices.SUCCESS
        payment.save(update_fields=["status"])
        return self.SUCCESS_URL

    @transaction.atomic
    def handle_successful_deposit_payment(self, payment):
        wallet = Wallet.objects.select_for_update().filter(id=payment.sender_wallet.id).first()
        wallet.deposit(amount=payment.amount)
        payment.status = choices.SUCCESS
        payment.save(update_fields=["status"])
        WalletLog.objects.create(wallet=wallet, amount=payment.amount, payment=payment)
        return self.SUCCESS_URL


bank_proxy = BankProxy()
