FROM ubuntu:22.04
RUN mkdir /app
WORKDIR /app
RUN apt-get update && apt-get install -y python3.9 python3-pip -y
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]