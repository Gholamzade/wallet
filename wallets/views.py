from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from payments.models import Payment
from services.proxies.bank_proxy import bank_proxy
from utils import choices
from wallets.models import Wallet
from wallets.serializers import WalletSerializer, CreateDepositSerializer, CreateScheduleWithdrawSerializer


class CreateWalletView(CreateAPIView):
    serializer_class = WalletSerializer


class RetrieveWalletView(RetrieveAPIView):
    serializer_class = WalletSerializer
    queryset = Wallet.objects.filter(is_active=True)
    lookup_field = "uuid"


class CreateDepositView(APIView):
    def post(self, reqeust, uuid, *args, **kwargs):
        serializer = CreateDepositSerializer(data=self.request.data)
        if serializer.is_valid():
            amount = serializer.data.get("amount")
            wallet = get_object_or_404(Wallet, uuid=uuid)
            payment = Payment.objects.create(
                amount=amount, sender_wallet=wallet, transaction_type=choices.TRANSACTION_TYPE_DEPOSIT
            )

            return Response({"data": bank_proxy.create_invoice(payment=payment)}, status=status.HTTP_201_CREATED)

        return Response({'message': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class ScheduleWithdrawView(APIView):
    def post(self, request, uuid, *args, **kwargs):
        sender_wallet = get_object_or_404(Wallet, uuid=uuid)
        serializer = CreateScheduleWithdrawSerializer(data=self.request.data, context={
            "sender_wallet": sender_wallet
        })
        if serializer.is_valid():
            amount = serializer.data.get("amount")
            must_done_at = serializer.validated_data.get("must_done_at")
            receiver_wallet_uuid = serializer.data.get("receiver_wallet_uuid")
            receiver_wallet = Wallet.objects.filter(uuid=receiver_wallet_uuid).first()
            payment = Payment.objects.create(
                amount=amount, sender_wallet=sender_wallet, transaction_type=choices.TRANSACTION_TYPE_WITHDRAW,
                must_done_at=must_done_at, receiver_wallet=receiver_wallet
            )

            return Response({"data": {"invoice_token": payment.invoice_token}}, status=status.HTTP_201_CREATED)

        return Response({'message': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
