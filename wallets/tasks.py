from django.utils.timezone import now

from payments.models import Payment
from services.proxies.bank_proxy import bank_proxy
from utils import choices
from wallet.celery import app


@app.task(bind=False)
def pay_future_withdraw(payment_id):
    payment = Payment.objects.filter(
        id=payment_id, status=choices.IN_PROGRESS, transaction_type=choices.TRANSACTION_TYPE_WITHDRAW,
        must_done_at__lte=now()
    ).first()
    if payment is None:
        return
    try:
        checked_data = bank_proxy.check(invoice__token=payment.invoice_token)
        status = checked_data.get("status")
        if status == "success":
            return bank_proxy.confirm_success_payment(payment=payment)
        return bank_proxy.confirm_failed_payment(payment=payment)
    except Exception as err:
        pass
