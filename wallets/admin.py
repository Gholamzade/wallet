from django.contrib import admin

from wallets.models import Wallet, WalletLog


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    fields = (
        "uuid",
        "balance",
        "created_at",
        "is_active",
        "modified_at",
    )
    readonly_fields = (
        "uuid",
        "balance",
        "created_at",
        "modified_at",
    )


@admin.register(WalletLog)
class WalletLogAdmin(admin.ModelAdmin):
    fields = (
        "wallet",
        "payment",
        "amount",
        "created_at",
        "is_active",
        "modified_at",
    )
    readonly_fields = (
        "amount",
        "created_at",
        "modified_at",
    )
