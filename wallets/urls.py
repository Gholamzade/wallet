from django.urls import path

from wallets.views import CreateDepositView, ScheduleWithdrawView, CreateWalletView, RetrieveWalletView

urlpatterns = [
    path("", CreateWalletView.as_view(), name="create_wallet"),
    path("<uuid>/", RetrieveWalletView.as_view(), name="get_wallet"),
    path("<uuid>/deposit", CreateDepositView.as_view(), name="deposit_to_wallet"),
    path("<uuid>/withdraw", ScheduleWithdrawView.as_view(), name="withdraw_from_wallet"),
]
