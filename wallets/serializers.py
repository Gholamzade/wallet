from django.utils.timezone import now
from rest_framework import serializers

from wallets.models import Wallet


class CreateDepositSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0, required=True)


class CreateScheduleWithdrawSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0, required=True)
    must_done_at = serializers.DateTimeField(required=True)
    receiver_wallet_uuid = serializers.UUIDField(required=True)

    def validate_amount(self, amount):
        sender_wallet = self.context.get("sender_wallet")
        if sender_wallet.unblocked_balance < amount:
            raise serializers.ValidationError("invalid amount")
        return amount

    def validate_receiver_wallet_uuid(self, receiver_wallet_uuid):
        sender_wallet = self.context.get("sender_wallet")

        receiver_wallet = Wallet.objects.filter(uuid=receiver_wallet_uuid).first()
        if receiver_wallet is None:
            raise serializers.ValidationError("invalid receiver wallet uuid")

        if receiver_wallet.id == sender_wallet.id:
            raise serializers.ValidationError("invalid receiver wallet uuid must be different with sender wallet")
        return receiver_wallet_uuid

    def validate_must_done_at(self, must_done_at):
        if now() > must_done_at:
            raise serializers.ValidationError("invalid future must done at")
        return must_done_at


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ("uuid", "balance", "unblocked_balance")
        read_only_fields = ("uuid", "balance", "unblocked_balance")
