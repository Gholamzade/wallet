import uuid

from django.db import models, transaction

from utils.base_model import BaseModel
from utils.exceptions import InvalidAmountException


class Wallet(BaseModel):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    balance = models.PositiveBigIntegerField(default=0)
    min_blocked_balance = models.BigIntegerField(default=0)

    def __str__(self):
        return str(self.uuid)

    @property
    def unblocked_balance(self):
        return self.balance - self.min_blocked_balance



    @transaction.atomic
    def deposit(self, amount: int):
        if amount > 0:
            self.balance += amount
            self.save(update_fields=["balance"])
        else:
            raise InvalidAmountException("amount must be positive.")

    @transaction.atomic
    def withdraw(self, amount: int):
        if amount > 0 and amount <= self.unblocked_balance:
            self.balance -= amount
            self.save(update_fields=["balance"])
        else:
            raise InvalidAmountException("amount must be positive and bigger than balance.")


class WalletLog(BaseModel):
    wallet = models.ForeignKey(
        Wallet, on_delete=models.PROTECT, related_name="wallet_logs"
    )

    payment = models.ForeignKey(
        "payments.Payment", on_delete=models.PROTECT, related_name="payments"
    )
    amount = models.PositiveBigIntegerField(default=0)

    def __str__(self):
        return self.id
