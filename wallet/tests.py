import datetime
import uuid
from unittest.mock import patch

from django.conf import settings
from django.db import IntegrityError, transaction
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now
from rest_framework import status

from payments.models import Payment
from utils import choices
from utils.exceptions import InvalidAmountException
from wallets.models import Wallet
from wallets.tasks import pay_future_withdraw


class WalletModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.wallet = Wallet.objects.create(balance=5000)

    def test_create_wallet_model_with_positive_balance_success(self):
        wallet_balance = 6000
        old_wallet_counts = Wallet.objects.count()

        wallet = Wallet.objects.create(balance=wallet_balance)
        new_wallet_counts = Wallet.objects.count()

        self.assertEqual(old_wallet_counts + 1, new_wallet_counts)

        self.assertEqual(wallet.balance, wallet_balance)
        self.assertIsNotNone(wallet.uuid)

    def test_create_wallet_model_with_negative_balance_failed(self):
        wallet_balance = -6000
        old_wallet_counts = Wallet.objects.count()

        try:
            with transaction.atomic():
                Wallet.objects.create(balance=wallet_balance)
        except IntegrityError:
            pass

        new_wallet_counts = Wallet.objects.count()
        self.assertEqual(old_wallet_counts, new_wallet_counts)

    def test_deposit_positive_amount_to_wallet_success(self):
        before_balance = self.wallet.balance
        amount = 6000
        self.wallet.deposit(amount=amount)
        self.wallet.refresh_from_db()
        self.assertEqual(before_balance + amount, self.wallet.balance)

    def test_deposit_negative_amount_to_wallet_failed(self):
        before_balance = self.wallet.balance
        amount = -100
        try:
            with transaction.atomic():
                self.wallet.deposit(amount=amount)

        except InvalidAmountException:
            pass

        self.wallet.refresh_from_db()
        self.assertEqual(before_balance, self.wallet.balance)

    def test_withdraw_amount_less_than_wallet_balance_success(self):
        before_balance = self.wallet.balance
        amount = 100
        self.wallet.withdraw(amount=amount)
        self.wallet.refresh_from_db()
        self.assertEqual(before_balance - amount, self.wallet.balance)

    def test_withdraw_amount_more_than_wallet_balance_failed(self):
        before_balance = self.wallet.balance
        amount = before_balance + 100
        try:
            with transaction.atomic():
                self.wallet.withdraw(amount=amount)

        except InvalidAmountException:
            pass

        self.wallet.refresh_from_db()
        self.assertEqual(before_balance, self.wallet.balance)


class WalletViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.wallet = Wallet.objects.create(balance=5000)
        cls.receiver_wallet = Wallet.objects.create(balance=5000)
        cls.mock_create_invoice_return_value = {
            "invoice_token": "fake_token",
            "amount": 2000,
            "deposit_url": "https://aa.com"
        }

    def test_create_wallet_view_with_zero_balance_201_success(self):
        url = reverse("create_wallet")
        response = self.client.post(url, data={}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.json().get("uuid"))
        self.assertEqual(response.json().get("balance"), 0)

    def test_create_wallet_view_with_positive_balance_failed(self):
        balance = 6000
        url = reverse("create_wallet")
        response = self.client.post(url, data={"balance": balance}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.json().get("uuid"))
        self.assertEqual(response.json().get("balance"), 0)
        self.assertNotEquals(response.json().get("balance"), balance)

    def test_create_wallet_view_with_uuid_failed(self):
        uuid_ = uuid.uuid4()
        url = reverse("create_wallet")
        response = self.client.post(url, data={"uuid": uuid_}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.json().get("uuid"))
        self.assertEqual(response.json().get("balance"), 0)
        self.assertNotEquals(response.json().get("uuid"), uuid_)

    def test_get_wallet_view_200_success(self):
        url = reverse("get_wallet", kwargs={"uuid": self.wallet.uuid})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get("uuid"), str(self.wallet.uuid))
        self.assertEqual(response.json().get("balance"), self.wallet.balance)

    def test_get_wallet_view_with_wrong_uuid_404_failed(self):
        url = reverse("get_wallet", kwargs={"uuid": uuid.uuid4()})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch("services.proxies.bank_proxy.bank_proxy.create_invoice")
    def test_deposit_positive_amount_into_wallet_view_200_success(self, mock_bank_create_invoice):
        mock_bank_create_invoice.return_value = self.mock_create_invoice_return_value
        amount = 2000
        url = reverse("deposit_to_wallet", kwargs={"uuid": self.wallet.uuid})
        response = self.client.post(url, data={"amount": amount}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json().get("data"), self.mock_create_invoice_return_value)
        payment = Payment.objects.filter(
            sender_wallet=self.wallet, amount=amount, transaction_type=choices.TRANSACTION_TYPE_DEPOSIT
        ).last()

        self.assertIsNotNone(payment)
        self.assertEqual(
            (payment.expired_at - payment.created_at).seconds // 60,
            settings.PAYMENT_EXPIRATION_IN_MINUTES
        )

    def test_deposit_negative_amount_into_wallet_view_400_failed(self):
        amount = -2000
        url = reverse("deposit_to_wallet", kwargs={"uuid": self.wallet.uuid})
        response = self.client.post(url, data={"amount": amount}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_deposit_into_wallet_view_with_wrong_uuid_404_failed(self):
        amount = 2000
        url = reverse("deposit_to_wallet", kwargs={"uuid": uuid.uuid4()})
        response = self.client.post(url, data={"amount": amount}, format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_withdraw_less_amount_than_wallet_view_200_success(self):
        amount = 20
        must_done_at = now() + datetime.timedelta(minutes=2)
        url = reverse("withdraw_from_wallet", kwargs={"uuid": self.wallet.uuid})
        data = {
            "amount": amount,
            "receiver_wallet_uuid": self.receiver_wallet.uuid,
            "must_done_at": must_done_at
        }
        response = self.client.post(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        payment_invoice_token = response.json().get("data", {}).get("invoice_token")
        payment = Payment.objects.filter(invoice_token=payment_invoice_token).first()

        self.assertIsNotNone(payment)
        self.assertEqual(payment.must_done_at, must_done_at)
        self.assertEqual(payment.sender_wallet, self.wallet)
        self.assertEqual(payment.receiver_wallet, self.receiver_wallet)
        self.assertEqual(payment.amount, amount)
        self.assertEqual(payment.transaction_type, choices.TRANSACTION_TYPE_WITHDRAW)
        self.assertEqual(payment.status, choices.IN_PROGRESS)
        self.assertEqual(
            (payment.expired_at - payment.must_done_at).seconds // 60,
            settings.PAYMENT_EXPIRATION_IN_MINUTES
        )

    def test_withdraw_more_amount_than_wallet_view_400_failed(self):
        amount = self.wallet.balance + 20
        must_done_at = now() + datetime.timedelta(minutes=2)
        url = reverse("withdraw_from_wallet", kwargs={"uuid": self.wallet.uuid})
        data = {
            "amount": amount,
            "receiver_wallet_uuid": self.receiver_wallet.uuid,
            "must_done_at": must_done_at
        }
        response = self.client.post(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_withdraw_from_wallet_view_with_past_must_done_at_400_failed(self):
        amount = 20
        must_done_at = now() - datetime.timedelta(minutes=2)
        url = reverse("withdraw_from_wallet", kwargs={"uuid": self.wallet.uuid})
        data = {
            "amount": amount,
            "receiver_wallet_uuid": self.receiver_wallet.uuid,
            "must_done_at": must_done_at
        }
        response = self.client.post(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PayFutureWithdrawTaskTestCase(TestCase):
    def setUp(self):
        self.sender_wallet = Wallet.objects.create(balance=4000)
        self.receiver_wallet = Wallet.objects.create(balance=5000)
        self.amount = 100
        self.payment = Payment.objects.create(
            sender_wallet=self.sender_wallet,
            receiver_wallet=self.receiver_wallet,
            transaction_type=choices.TRANSACTION_TYPE_WITHDRAW,
            amount=self.amount,
            status=choices.IN_PROGRESS, expired_at=now() + datetime.timedelta(minutes=7),
            must_done_at=now()
        )
        self.mock_checked_data_return_value = {
            "status": "success",
            "amount": self.amount,
        }

    @patch("services.proxies.bank_proxy.bank_proxy.check")
    def test_task_pay_future_withdraw_success(self, mock_check):
        mock_check.return_value = self.mock_checked_data_return_value
        self.assertEqual(self.payment.status, choices.IN_PROGRESS)
        old_sender_wallet_balance = self.sender_wallet.balance
        old_receiver_wallet_balance = self.receiver_wallet.balance

        pay_future_withdraw(payment_id=self.payment.id)
        self.payment.refresh_from_db()
        self.sender_wallet.refresh_from_db()
        self.receiver_wallet.refresh_from_db()
        self.assertEqual(self.payment.status, choices.SUCCESS)
        self.assertEqual(self.sender_wallet.balance, old_sender_wallet_balance - self.amount)
        self.assertEqual(self.receiver_wallet.balance, old_receiver_wallet_balance + self.amount)
