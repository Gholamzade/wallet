from rest_framework import serializers


class BankProxyCallbackSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0, required=True)
    status = serializers.CharField(required=True)
    invoice_token = serializers.UUIDField(required=True)
