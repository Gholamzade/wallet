from django.urls import path

from payments.views import BankProxyCallbackView

urlpatterns = [
    path("fake-bank/callback", BankProxyCallbackView.as_view()),

]
