import datetime

from django.test import TestCase
from django.utils.timezone import now

from payments.models import Payment
from payments.tasks import make_payments_expire
from utils import choices
from wallets.models import Wallet


class PaymentExpirationTaskTestCase(TestCase):
    def setUp(self):
        wallet = Wallet.objects.create()
        self.payment_with_old_expiration = Payment.objects.create(
            sender_wallet=wallet,
            transaction_type=choices.TRANSACTION_TYPE_DEPOSIT,
            status=choices.IN_PROGRESS, expired_at=now() - datetime.timedelta(minutes=7)
        )
        self.payment_with_future_expiration = Payment.objects.create(
            sender_wallet=wallet,
            transaction_type=choices.TRANSACTION_TYPE_WITHDRAW,

            status=choices.IN_PROGRESS, expired_at=now() + datetime.timedelta(minutes=7)
        )

    def test_task_make_payments_expire_with_one_old_and_one_future_expired_at(self):
        self.assertEqual(self.payment_with_old_expiration.status, choices.IN_PROGRESS)
        self.assertEqual(self.payment_with_future_expiration.status, choices.IN_PROGRESS)
        make_payments_expire()
        self.payment_with_old_expiration.refresh_from_db()
        self.payment_with_future_expiration.refresh_from_db()
        self.assertEqual(self.payment_with_old_expiration.status, choices.FAILED)
        self.assertEqual(self.payment_with_future_expiration.status, choices.IN_PROGRESS)
