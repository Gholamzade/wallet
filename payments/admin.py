from django.contrib import admin

from payments.models import Payment, Transaction


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    fields = (
        "status",
        "amount",
        "invoice_token",
        "must_done_at",
        "sender_wallet",
        "receiver_wallet",
        "created_at",
        "is_active",
        "modified_at",
        "expired_at",
    )
    readonly_fields = (
        "amount",
        "invoice_token",
        "created_at",
        "modified_at",
    )


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    pass
