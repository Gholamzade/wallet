# Generated by Django 3.2 on 2024-05-22 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_alter_payment_receiver_wallet'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='expired_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
