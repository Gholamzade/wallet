from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from payments.serializers import BankProxyCallbackSerializer
from services.proxies.bank_proxy import bank_proxy


class BankProxyCallbackView(APIView):

    def post(self, reqeust, *args, **kwargs):
        try:
            serializer = BankProxyCallbackSerializer(data=reqeust.data)
            serializer.is_valid(raise_exception=True)
            link = bank_proxy.confirm_invoice(reqeust.data)
            return Response({"data": link}, status=status.HTTP_200_OK)
        except Exception as err:
            return Response({"error": str(err)}, status=status.HTTP_400_BAD_REQUEST)
