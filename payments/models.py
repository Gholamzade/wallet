import uuid
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils.timezone import now

from utils import choices
from utils.base_model import BaseModel
from wallets.models import Wallet


class Payment(BaseModel):
    amount = models.PositiveBigIntegerField(default=0)
    transaction_type = models.PositiveSmallIntegerField(choices=choices.TRANSACTION_TYPES_CHOICES)
    status = models.PositiveSmallIntegerField(choices=choices.STATUS_CHOICES, default=choices.IN_PROGRESS)
    invoice_token = models.UUIDField(default=uuid.uuid4, unique=True)
    must_done_at = models.DateTimeField(null=True, blank=True)  # for future withdraw
    expired_at = models.DateTimeField(null=True, blank=True)  # for deposit
    sender_wallet = models.ForeignKey(
        Wallet, on_delete=models.PROTECT, related_name="sender_wallets"
    )
    receiver_wallet = models.ForeignKey(
        Wallet, on_delete=models.PROTECT, related_name="receiver_wallets", null=True, blank=True
    )

    def __str__(self):
        return str(self.invoice_token)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        is_created = False
        if self.id is None:
            is_created = True
        if self.expired_at is None:
            self.expired_at = now() + timedelta(minutes=settings.PAYMENT_EXPIRATION_IN_MINUTES)
            if self.transaction_type == choices.TRANSACTION_TYPE_WITHDRAW:
                self.expired_at = self.must_done_at + timedelta(minutes=settings.PAYMENT_EXPIRATION_IN_MINUTES)

            if update_fields:
                update_fields.append("expired_at")

        response = super().save(
            force_insert=force_insert, force_update=force_update, using=using,
            update_fields=update_fields
        )
        if is_created and self.transaction_type == choices.TRANSACTION_TYPE_WITHDRAW:
            self.create_future_withdraw_task()
        return response

    def create_future_withdraw_task(self):
        from wallets.tasks import pay_future_withdraw
        pay_future_withdraw.apply_async((self.id,), eta=self.must_done_at)


class Transaction(BaseModel):
    amount = models.PositiveBigIntegerField()
    status = models.PositiveSmallIntegerField(choices=choices.STATUS_CHOICES, default=choices.IN_PROGRESS)
    invoice_token = models.UUIDField(default=uuid.uuid4, unique=True)
    payment = models.ForeignKey(
        Payment, on_delete=models.PROTECT, related_name="transactions"
    )

    def __str__(self):
        return str(self.invoice_token)
