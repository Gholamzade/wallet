from django.utils.timezone import now

from payments.models import Payment
from utils import choices
from wallet.celery import app


@app.task(bind=False)
def make_payments_expire():
    Payment.objects.filter(expired_at__lt=now(), status=choices.IN_PROGRESS).update(status=choices.FAILED)

