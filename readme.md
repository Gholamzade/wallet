## Installation
for running this project, first of all you must run below command.
```bash
pip install -r requrements.txt
```
## Running Workers

after that you must run below command in services.proxies folder.
```bash
python bank_worker.py
```
make sure your redis server must have started.
after that run celery beat with this command: 
```bash
celery -A wallet beat -l info
```
and run celery worker with this command: 
```bash
celery -A wallet worker -l info --without-gossip --without-mingle --without-heartbeat -O
fair --pool=solo
```

# Wallet Service
you can make a wallet for yourself in this project and see its detail.
you can deposit and future withdraw money with imaginary and fake bank that its proxy located in services.proxies.bank_proxy.py.
in bank_proxy.py there is real bank api calling simulation and its check function, 
we send request to async simple_request. Wallet and Wallet Log model are located in wallets app and payment model are located in payments app.
## how deposit work?
in this case we make in progress payment with expired time in PAYMENT_EXPIRATION_IN_MINUTES minutes. 
when we get success callback from fake bank, we make its payment success else we make it Failed. 
if we didn't receive any callback, we will expire that payment after PAYMENT_EXPIRATION_IN_MINUTES minutes. 
'make_payments_expire' is a beat celery task that is running every minute to make expire these failed payments.

## how future withdraw work?
in this case we make in progress payment with future time must done at and expired time 
PAYMENT_EXPIRATION_IN_MINUTES minutes after than must done at. 
we make a future celery task to run pay_future_withdraw task at determined time.
when pay_future_withdraw run, we send a request to fake bank check function and wait to get its response. 

if it has a success response its payment success we call confirm_success_payment in bank proxy 
else we call confirm_failed_payment instead.

## is there any test for this system?
yes, there is wallet model test, wallet api like create, get, deposit and withdraw tests in wallets.tests.py file. 
in deposit and withdraw tests we mock bank proxy to test our rest of code.
we test task make_payments_expire in payment.tests and pay_future_withdraw in wallets.test.